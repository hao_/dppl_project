#include "util.h"

#ifndef CBIF_H
#define CBIF_H

#define BOTTOM_DATA_TYPE unsigned

#define FIELDNAME(x) _ ## x
#define FIELDMAP_B(x, bit) BOTTOM_DATA_TYPE FIELDNAME(x) : bit;
#define FILEDMAP_ENTRY(C) FIELDMAP_ ## C

#define STRUCTDEF(type_name, ...) struct type_name { \
									ARGSMAP_ENTRY(FILEDMAP_ENTRY, __VA_ARGS__) \
};

/* Interface */

#define CBIF_DEFINE(type_name, ...) STRUCTDEF(type_name, __VA_ARGS__)

#define CBIF_NEW(type_name) {}

#define CBIT(obj, bit) (obj.FIELDNAME(bit))

#define CASGNI(obj, bit, value) { \
	obj.FIELDNAME(bit) = (value); \
} while(0);;

#define CASGN(obj, bit, value) CASGNI(obj, bit, value);


#define CINT(x) x

#endif
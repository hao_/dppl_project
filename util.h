#ifndef UTIL_H
#define UTIL_H

#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__

#define COMPL(b) PRIMITIVE_CAT(COMPL_, b)
#define COMPL_0 1
#define COMPL_1 0

#define CHECK_N(x, n, ...) n
#define CHECK(...) CHECK_N(__VA_ARGS__, 0,)
#define PROBE(x) x, 1,

#define IS_PAREN(x) CHECK(IS_PAREN_PROBE x)
#define IS_PAREN_PROBE(...) PROBE(~)

#define NOT(x) CHECK(PRIMITIVE_CAT(NOT_, x))
#define NOT_0 PROBE(~)

#define COMPL(b) PRIMITIVE_CAT(COMPL_, b)
#define COMPL_0 1
#define COMPL_1 0

#define BOOL(x) COMPL(NOT(x))

#define IIF(c) PRIMITIVE_CAT(IIF_, c)
#define IIF_0(t, ...) __VA_ARGS__
#define IIF_1(t, ...) t

#define IF(c) IIF(BOOL(c))

#define EAT(...)
#define EXPAND(...) __VA_ARGS__

#define EMPTY()
#define DEFER(id) id EMPTY()
#define OBSTRUCT(id) id DEFER(EMPTY)()

#define EVAL(...)  EVAL1(EVAL1(EVAL1(__VA_ARGS__)))
#define EVAL1(...) EVAL2(EVAL2(EVAL2(__VA_ARGS__)))
#define EVAL2(...) EVAL3(EVAL3(EVAL3(__VA_ARGS__)))
#define EVAL3(...) EVAL4(EVAL4(EVAL4(__VA_ARGS__)))
#define EVAL4(...) EVAL5(EVAL5(EVAL5(__VA_ARGS__)))
#define EVAL5(...) __VA_ARGS__

#define TAKE_FIRST(a, ...) a

#define ARGSMAP(MAPPER, field, ...) \
	MAPPER(field) \
	IF(TAKE_FIRST(__VA_ARGS__)) \
		( \
	    OBSTRUCT(ARGSMAP_INDIRECT) () \
	    ( \
	        MAPPER, __VA_ARGS__ \
	    ) \
	)
#define ARGSMAP_INDIRECT() ARGSMAP
#define ARGSMAP_ENTRY(MAPPER, ...) EVAL(ARGSMAP(MAPPER, __VA_ARGS__, 0))

#define ARGSMAP_ARG(MAPPER, arg, field, ...) \
	MAPPER(arg, field) \
	IF(TAKE_FIRST(__VA_ARGS__)) \
		( \
	    OBSTRUCT(ARGSMAP_ARG_INDIRECT) () \
	    ( \
	        MAPPER, arg, __VA_ARGS__ \
	    ) \
	)
#define ARGSMAP_ARG_INDIRECT() ARGSMAP_ARG
#define ARGSMAP_ARG_ENTRY(MAPPER, arg, ...) EVAL(ARGSMAP_ARG(MAPPER, arg, __VA_ARGS__, 0))

#endif
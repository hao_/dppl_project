ipath := .
testsdir := tests
objdir := obj

tests: $(objdir)/typing $(objdir)/cbif_lite $(objdir)/test $(objdir)/size

tests_error: typing_error bfdef_error bignum_error notinit_error bignum_runtime_error

typing_error: $(objdir)/typing_error
bfdef_error: $(objdir)/bfdef_error
bignum_error: $(objdir)/bignum_error
notinit_error: $(objdir)/notinit_error
bignum_runtime_error: $(objdir)/bignum_runtime_error
maxbfs: $(objdir)/maxbfs


$(objdir)/%: $(testsdir)/%.c $(objdir)
	-$(CC) $< -o $@ -g -O2 -Werror -Wall -I $(ipath)

$(objdir):
	mkdir $(objdir)

.PHONY: clean
clean:
	rm -rf $(objdir)
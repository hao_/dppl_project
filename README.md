# Typed CBi(t)F(ield)

## Usage
```
#include <cbif_strict.h> // strict typed bit field
#include <cbif.h> // efficient bit field

// Define a bitfield type
CBIF_DEFINE(la , B(pdx, 10), B(ptx, 10), B(pgoff, 12))

struct la la1 = CBIF_NEW(la);
struct la la2 = CBIF_NEW(lb);

// Assign an integer: la1.pdx = 4;
CASGNI(la1, pdx, 4);

// Assign a bit field with same name: la1.pdx = la2.pdx;
CASGN(la1, pdx, CBIT(la2, pdx));

// Assign a bit field with different name: la1.pdx = la2.ptx
CASGN(la1, pdx, CBIT(la2, ptx)); // CE
CASGNI(la1, pdx, CINT(CBIT(la2, ptx))); // Right
```

## Todo
- Make runtime check optional
- An easy to use constructor for bit field

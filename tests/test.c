#include <stdio.h>
#include "cbif.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

// bit <= 0 or > 32
//CBIF_DEFINE(_tp1, B(a, 0))
//CBIF_DEFINE(_tp2, B(a, 33))

int Donothing(int *t) {
	int tmp;
	tmp = 1;
	return tmp;
}
struct tp_c_original {
	int a : 4;
	int b : 5;
};

int main() {
	/* Test */
	struct tp a = CBIF_NEW(tp);
	struct tp_c_original a_orginal;

	CASGNI(a, a, 3);
	CASGNI(a, b, 10);
	printf("%d\n", CINT(CBIT(a, a)));
	CASGN(a, a, CBIT(a, a));
	CASGNI(a, a, CINT(CBIT(a, b)))
	printf("%d\n", CINT(CBIT(a, a)));

	//CASGNI(a, a, 30000); // Compile warning: value exceeded

	/*
	// Runtime error: bit field not initilized with CBIF_NEW
	struct tp b;
	CASGNI(b, b, 3);
	*/
	
	// Runtime error: value exceeded
	int tmp = 30000;
	Donothing(&tmp);
	a_orginal.a = tmp;
	tmp = a_orginal.a;
	//CASGNI(a, a, tmp);
	

	// CASGN(a, a, CBIT(a, b));  // Compile error: assign tp.b to tp.a
	return 0;
}
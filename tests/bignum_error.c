#include <stdio.h>
#include "cbif_strict.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

int main() {
	/* Typing cbif */
	struct tp bf = CBIF_NEW(tp);

	CASGNI(bf, a, 300000);
	return 0;
}
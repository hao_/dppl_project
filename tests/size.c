#include <stdio.h>
#include "cbif.h"

CBIF_DEFINE(tp , B(a, 28), B(b, 5), B(c, 10), B(d, 23), B(e, 24))

struct tp_ori {
	unsigned a : 28;
	unsigned b : 5;
	unsigned c : 10;
	unsigned d : 23;
	unsigned e : 24;
};

int main() {
	printf("cbif size: %lu\n", sizeof(struct tp));
	printf("c bit field size: %lu\n", sizeof(struct tp_ori));
	return 0;
}
#include <stdio.h>
#include "cbif_strict.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

struct tp_ori {
	unsigned a : 4;
	unsigned b : 5;
};

int main() {
	/* Typing cbif */
	struct tp bf = CBIF_NEW(tp);

	CASGN(bf, a, CBIT(bf, b));
	return 0;
}
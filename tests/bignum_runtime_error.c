#include <stdio.h>
#include "cbif_strict.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

int Donothing(int *t) {
	int tmp;
	tmp = 1;
	return tmp;
}

struct tp_c_original {
	int a : 4;
	int b : 5;
};

int main() {
	/* Test */
	struct tp a = CBIF_NEW(tp);
	struct tp_c_original a_orginal;

	// Runtime error: value exceeded
	int tmp = 30005;
	a_orginal.a = tmp;
	printf("%d\n", a_orginal.a);
	CASGNI(a, a, tmp);
	printf("%d\n", CINT(CBIT(a,a)));
	return 0;
}
#include <stdio.h>
#include "cbif_strict.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

int main() {
	/* Typing cbif */
	struct tp a = CBIF_NEW(tp); // TO prevent unused warning
	struct tp bf;

	CASGNI(bf, a, 1); // Runtime error
	CASGNI(a, a, 10);
	return 0;
}
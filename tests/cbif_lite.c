#include <stdio.h>
#include "cbif.h"

CBIF_DEFINE(tp , B(a, 4), B(b, 5))

struct tp_ori {
	unsigned a : 4;
	unsigned b : 5;
};

int main() {
	/* Typing cbif */
	struct tp bf = CBIF_NEW(tp);

	CASGNI(bf, a, 3);
	CASGNI(bf, b, 10);
	printf("%d\n", CINT(CBIT(bf, a)));

	CASGN(bf, a, CBIT(bf, a));
	CASGNI(bf, a, CINT(CBIT(bf, b)))
	printf("%d\n", CINT(CBIT(bf, a)));

	/* C original bit field */
	struct tp_ori bf_ori;
	bf_ori.a = 3;
	bf_ori.b = 10;
	printf("%d\n", bf_ori.a);

	bf_ori.a = bf_ori.b;
	printf("%d\n", bf_ori.a);
	return 0;
}
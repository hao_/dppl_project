#include <assert.h>
#include "util.h"

#ifndef CBIF_STRICT_H
#define CBIF_STRICT_H

/* Specification */
#define BOTTOM_DATA_TYPE unsigned
#define CBIF_INNER_TYPE_PREFIX __cbif_strict_
#define CBIF_CONSTANT_BIT_PREFIX CBIF_INNER_TYPE_PREFIX
#define CBIF_BITVAR_NAME bit
#define CBIF_VALUEVAR_NAME value

/* Name Helper */

#define GET_FIELDMAP_TYPE_B(x, bit) x
#define GET_FIELDMAP_TYPE(C) GET_FIELDMAP_TYPE_ ## C
#define GET_FIELDMAP_BIT_B(x, bit) bit
#define GET_FIELDMAP_BIT(C) GET_FIELDMAP_BIT_ ## C
#define STRICT_TYPE_LABEL_INNER(prefix, type, x) struct prefix ## type ## _ ## x
#define STRICT_TYPE_LABEL_INNER2(prefix, type, x) STRICT_TYPE_LABEL_INNER(prefix, type, x)
#define STRICT_TYPE_LABEL(type, x) STRICT_TYPE_LABEL_INNER2(CBIF_INNER_TYPE_PREFIX, type, x)

/* Main struct definition */
#define STRICT_FIELDMAP_VAR(x) _ ## x
#define STRICT_FIELDMAP_VARDEF_B(x, bit) STRICT_FIELDMAP_VAR(x);

#define STRICT_FIELDMAP_TYPEWRAPER(type_name, C) STRICT_TYPE_LABEL(type_name, GET_FIELDMAP_TYPE(C)) \
													STRICT_FIELDMAP_VARDEF_ ## C

// Guard is for compile-time check
#define STRICT_FIELDMAP_GUARD_NAME(x) _guard_ ## x
#define STRICT_FIELDMAP_GUARD_NAME_B(x, bit) _guard_ ## x
#define STRICT_FIELDMAP_GUARD_NAME_FROMB(C) STRICT_FIELDMAP_GUARD_NAME_ ## C
#define STRICT_FIELDMAP_GUARD(C) BOTTOM_DATA_TYPE STRICT_FIELDMAP_GUARD_NAME_FROMB(C) : GET_FIELDMAP_BIT(C);

#define STRICT_STRUCTDEF(type_name, ...) struct type_name { \
									ARGSMAP_ARG_ENTRY(STRICT_FIELDMAP_TYPEWRAPER, type_name, __VA_ARGS__) \
									ARGSMAP_ENTRY(STRICT_FIELDMAP_GUARD, __VA_ARGS__) \
};

/* Runtime constant definition */
#define STRICT_CONSTANT_BIT_VARNAME_INNER(prefix, type_name, x) prefix ## type_name ## _ ## x ## _ ## bitlimit
#define STRICT_CONSTANT_BIT_VARNAME_INNER2(prefix, type_name, x) STRICT_CONSTANT_BIT_VARNAME_INNER(prefix, type_name, x)
#define STRICT_CONSTANT_BIT_VARNAME(type_name, x) STRICT_CONSTANT_BIT_VARNAME_INNER2(CBIF_CONSTANT_BIT_PREFIX, type_name, x)
#define STRICT_CONSTANT_BIT(type_name, C) static const BOTTOM_DATA_TYPE STRICT_CONSTANT_BIT_VARNAME(type_name, GET_FIELDMAP_TYPE(C)) \
																		 = GET_FIELDMAP_BIT(C);

#define STRICT_CONSTANTDEF(type_name, ...) //ARGSMAP_ARG_ENTRY(STRICT_CONSTANT_BIT, type_name, __VA_ARGS__)

/* Inner struct */
#define STRICT_FIELDTYPE_STRUCTDEF(type_name, C) STRICT_TYPE_LABEL(type_name, GET_FIELDMAP_TYPE(C)) { \
	BOTTOM_DATA_TYPE CBIF_BITVAR_NAME; \
	BOTTOM_DATA_TYPE CBIF_VALUEVAR_NAME; \
};
#define STRICT_INNERTYPEDEF(type_name, ...) ARGSMAP_ARG_ENTRY(STRICT_FIELDTYPE_STRUCTDEF, type_name, __VA_ARGS__)

/* Template: to implement CBIF_NEW */

#define STRICT_TEMPLATE_NAME_INNER(type_name) __cbif_template_ ## type_name
#define STRICT_TEMPLATE_NAME(type_name) STRICT_TEMPLATE_NAME_INNER(type_name)

#define STRICT_TEMPLATE_DEF(C) {GET_FIELDMAP_BIT(C)},
#define STRICT_TEMPLATE(type_name, ...) static const struct type_name STRICT_TEMPLATE_NAME(type_name) = { \
	ARGSMAP_ENTRY(STRICT_TEMPLATE_DEF, __VA_ARGS__) \
};


/* Static check: no need now, guard in main struct can replace it */
/*
#define STRICT_STATIC_CHECK_BITFIELD(C) static_assert(GET_FIELDMAP_BIT(C) > 0, "bit number should > 0"); \
          								static_assert(GET_FIELDMAP_BIT(C) < 32, "bit number should < 32");*/
#define STRICT_STATIC_CHECK(type_name, ...) /*ARGSMAP_ENTRY(STRICT_STATIC_CHECK_BITFIELD, __VA_ARGS__)*/

/* Interface */

#define CBIF_DEFINE(type_name, ...) STRICT_STATIC_CHECK(type_name, __VA_ARGS__) \
									STRICT_INNERTYPEDEF(type_name, __VA_ARGS__) \
									STRICT_CONSTANTDEF(type_name, __VA_ARGS__) \
									STRICT_STRUCTDEF(type_name, __VA_ARGS__) \
									STRICT_TEMPLATE(type_name, __VA_ARGS__)

#define CBIF_NEW(type_name) (STRICT_TEMPLATE_NAME(type_name))

#define CBIT(obj, bit) (obj.STRICT_FIELDMAP_VAR(bit))

#define CASGNI(obj, bit, value) { \
	typeof(obj.STRICT_FIELDMAP_VAR(bit).CBIF_VALUEVAR_NAME) __cbif_tmp = (value); \
	if (0) obj.STRICT_FIELDMAP_GUARD_NAME(bit) = (value); /* For compile-time check, never run */ \
	assert((obj.STRICT_FIELDMAP_VAR(bit).CBIF_BITVAR_NAME) > 0 && "bit field not initilized"); \
	assert(__cbif_tmp < (1 << (obj.STRICT_FIELDMAP_VAR(bit).CBIF_BITVAR_NAME)) && "value exceeds bit field size"); \
	obj.STRICT_FIELDMAP_VAR(bit).CBIF_VALUEVAR_NAME = __cbif_tmp; \
} while(0);

#define CASGN(obj, bit, value) { \
	assert((obj.STRICT_FIELDMAP_VAR(bit).CBIF_BITVAR_NAME) > 0 && "bit field not initilized"); \
	typeof(obj.STRICT_FIELDMAP_VAR(bit)) __cbif_tmp = (value); \
	obj.STRICT_FIELDMAP_VAR(bit) = __cbif_tmp; \
} while(0);

#define CBIF_INT_INNER(x, v) x.v
#define CINT(x) (CBIF_INT_INNER(x, CBIF_VALUEVAR_NAME))

#endif